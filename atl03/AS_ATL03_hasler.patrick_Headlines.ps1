<#
.SYNOPSIS
  Get the top headlines for Switzerland from https://newsapi.org

.DESCRIPTION
  Erstellt ein Skript, welches...
  2.1. alle aktuellen Schlagzeilen (Top Headlines) der Schweiz mittels Web-Request herunterlädt
  2.2. die Properties der Artikel source.name, title und url am Speicherort des Skripts in eine CSV-Datei exportiert.

.NOTES
  Author:   Patrick Hasler
  Datum:    06.06.2021
  Version:  1.0
#>

$apiKey="3fdfb0d729d444ff98a4e3c6385b5b6a"
$newsURL="https://newsapi.org/v2/top-headlines?country=ch&apiKey=$apiKey"

$apiCall = Invoke-RestMethod -Uri $newsURL

#2.1. alle aktuellen Schlagzeilen (Top Headlines) der Schweiz mittels Web-Request herunterlädt
$apiCall.articles | Format-Table -Property @{Name="Source"; Expression={$_.source.name}}, Title, Url

#2.2. die Properties der Artikel source.name, title und url am Speicherort des Skripts in eine CSVDatei exportiert
$apiCall.articles | Select-Object -Property @{Name="Source"; Expression={$_.source.name}}, Title, Url | Export-Csv -Path "./topHeadlines.csv" -NoTypeInformation