param (

  [Parameter(Mandatory=$true,
  ParameterSetName="Path")]
  [String[]]
  $dPath,

  [Parameter(Mandatory=$true,
  ParameterSetName="Firstname")]
  [String[]]
  $firstName,

  [Parameter(Mandatory=$true,
  ParameterSetName="Lastname")]
  [String[]]
  $lastName,

  [Parameter(Mandatory=$false,
  ParameterSetName="Departement")]
  [String[]]
  $departement = "Buchhaltung"

)

function createMail($firstName, $lastName, $departement, $domain){
  $email = $firstName + "." + $lastName + "@" + $departement + "." + $domain
  $email = $email.ToLower() 
  writeToFile "the address $email was created"
  Write-Output "the address $email was created"
}

function createShortName($firstName, $lastName){
  $shortName = $firstName.Remove(1) + $lastName.Remove(2)
  $shortName = $shortName.ToLower() 
  writeToFile "Firstname: $firstName, Lastname: $lastName, Shortname: $shortName"
  Write-Output "Firstname: $firstName, Lastname: $lastName, Shortname: $shortName"
}

function writeToFile($content){
   Add-Content -Path $dPath -Value $content
}

$domain = "acme.com"
createMail $firstName $lastName $departement $domain
createShortName $firstName $lastName