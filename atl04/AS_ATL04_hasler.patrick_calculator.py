#!/usr/bin/env python3

class Calculator:
  def addition(x, y):
    return x + y

  def subtraction(x, y):
    return x - y

  def multiply(x, y):
    return x * y

  def divide(x, y):
    try:
      return x / y
    except ZeroDivisionError:
      print("cannot divide by 0")

  def sum(*numbers):
    summary = 0
    for i in numbers:
      summary += i
    return summary

  def average(*numbers):
    numberList = []
    for i in numbers:
      numberList.append(i)
    return sum(numberList) / len(numberList)
