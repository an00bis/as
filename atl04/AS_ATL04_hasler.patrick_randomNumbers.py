#!/usr/bin/env python3

import random

numberlist = random.sample(range(0, 10000), 10)

for i in numberlist:
  if i % 2 == 0:
      print("gerade Zahl: ", i)
  else:
      print("ungerade Zahl: ", i)
