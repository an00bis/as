#!/usr/bin/env python3

number = int(input("Number: "))

for i in range(0, number+1):
  if i == 0:
    print(i)
  elif i % 15 == 0:
    print("Fizz Buzz")
  elif i % 5 == 0:
    print("Buzz")
  elif i % 3 == 0:
    print("Fizz")
  else:
    print(i)
