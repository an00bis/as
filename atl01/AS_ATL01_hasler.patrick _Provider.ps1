# 5.1. Listet alle Einträge von `HKEY_LOCAL_MACHINE/Software` auf. 
Get-ChildItem -Path "Registry::HKEY_LOCAL_MACHINE:\Software" -Recurse

# 5.2. Erstellt einen neuen Unterschlüssel „ACME Inc“ 
New-Item -Path "Registry::HKEY_LOCAL_MACHINE:\Software" -Name "ACME Inc"

# 5.3. Wechselt in den soeben erstellten Unterschlüssel 
Set-Location -Path "Registry::HKEY_LOCAL_MACHINE:\Software\ACME_Inc"

# 5.4. Erstellt ein neues PS\_Laufwerk namens `ACME:` für diesen Pfad 
New-PSDrive -Name ACME -PSProvider Registry -Root "Registry::HKEY_LOCAL_MACHINE:\Software\ACME_Inc"

# 5.5. Wechselt in das neue Laufwerk 
Set-Location -Path "ACME:"

# 5.6. Legt einen neuen Eintrag vom Typ `REG_SZ` names `Zeichenkette1` und Wert `Hallo Welt` an 
New-ItemProperty -Path "ACME:" -Name "Zeichenkette1" -PropertyType "String" -Value "Hallo Welt"

# 5.7. Kopiert o.g. Eintrag nach «Zeichenkette»
New-ItemProperty -Path "ACME:" -Name "Zeichenkette"
Copy-ItemProperty -Path "ACME:" -Name "Zeichenkette1" -Destination "ACME:\Zeichenkette"

# 5.8. Benennt den Eintrag 'Zeichenkette' um in 'Zeichenkette 2'
Rename-ItemProperty -Path "ACME:" -Name "Zeichenkette" -NewName "Zeichenkette2"

# 5.9. Ändert die Daten des Werts 'Zeichenkette2' in 'PowerShell ist toll.'.
Set-ItemProperty -Path "ACME:"-Name "Zeichenkette2" -Value "PowerShell ist toll."

# 5.10. Gibt alle Werte im Schluessel 'ACME Inc:' aus.
Get-ItemPropertyValue -Path "ACME:" -Name "Zeichenkette1", "Zeichenkette2"

# 5.11.  Löscht alle oben eingerichteten Eintraege. 
Remove-ItemProperty -Path "ACME:" -Name "Zeichenkette1", "Zeichenkette2"

# 5.12. Zeigt alle Laufwerke an.
Get-PSDrive

# 5.13. Löscht das Laufwerk 'ACME:'.
Remove-PSDrive -Name "ACME" -Force

# 5.14. Löscht den Unterschluessel 'ACME Inc'.
Remove-Item -Path "HKEY_LOCAL_MACHINE:\Software\ACME Inc"