<#
Jede durch drei teilbare Zahl wird durch das Wort „fizz“ und jede durch fünf teilbare Zahl durch das Wort „buzz“ ersetzt. 
Durch 15 teilbare Zahlen werden zu „fizz buzz“
#>

$input = [Int]$args[0]

for ( $i = 0 ; $i -le $input ; $i++ ){
  if( $i -eq 0 ){
    Write-Output "$i"
  }
  elseif( $i % 15 -eq 0 ){
    Write-Output "Fizz Buzz"
  } 
  elseif( $i % 5 -eq 0 ){
    Write-Output "Buzz"
  }
  elseif( $i % 3 -eq 0 ){
    Write-Output "Fizz"
  }
  else{
    Write-Output "$i"
  }
}