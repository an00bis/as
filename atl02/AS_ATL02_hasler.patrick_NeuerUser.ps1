<#
.SYNOPSIS
    Create new user
.DESCRIPTION
    Usage: Create a new employee user account.
.PARAMETER dPath
    Destination Path to CSV file (mandatory).
.PARAMETER firstName
    Firstname of new user (mandatory).
.PARAMETER lastName
    Lastnname of new user (mandatory).
.PARAMETER department
    Department of new user (optional. "Buchhaltung" is default).
.EXAMPLE
    AS_ATL02_hasler.patrick_NeuerUser.ps1 -dPath "./example.csv" -firstName "Volker" -lastName "Friedrich"
.NOTES
    Author: Patrick Hasler
    Date: 20.05.2021
    Version: 1.0
#>

param (
    [Parameter(Mandatory=$true)][String]$dPath,
    [Parameter(Mandatory=$true)][String]$firstName,
    [Parameter(Mandatory=$true)][String]$lastName,
    [String]$department = "Buchhaltung"
)

function createMail{
  $email = $firstName + "." + $lastName + "@" + $department + "." + $domain
  $email = $email.ToLower() 
  return $email
}

function createShortName{
  $shortName = $firstName.Remove(1) + $lastName.Remove(2)
  $shortName = $shortName.ToLower() 
  return $shortName
}

function exportToFile {
  $newUser = @{
    Firstname = $firstName
    Lastname = $lastName
    Department = $department
    Email = $email
    Shortname = $shortName
    Phonenumber = $phoneNumber
    }
    $newUser.GetEnumerator() | Export-Csv -Path "$dPath" -NoTypeInformation -Append
}

function generatePhoneNumber {
  $phoneNumber = Get-Random -Minimum 1201 -Maximum 1299
  return $phoneNumber
}

$domain = "acme.com"
$email = createMail
Write-Output "the address $email was created"
$shortName = createShortName
Write-Output "Firstname: $firstName, Lastname: $lastName, Shortname: $shortName"
$phoneNumber = generatePhoneNumber
exportToFile

Get-Content -Path $dPath